part of 'audio_player_cubit.dart';

sealed class AudioPlayerState {
  final double volume;

  const AudioPlayerState({
    this.volume = 0.0,
  });
}

class AudioPlayerStateInitial extends AudioPlayerState {
  const AudioPlayerStateInitial();
}

class AudioPlayerStateDisplayScreensaverInfo extends AudioPlayerState {
  const AudioPlayerStateDisplayScreensaverInfo();
}

class AudioPlayerStatePlaying extends AudioPlayerState {
  const AudioPlayerStatePlaying({required super.volume});
}

class AudioPlayerStatePlayingWithScreensaver extends AudioPlayerStatePlaying {
  const AudioPlayerStatePlayingWithScreensaver({required super.volume});
}

class AudioPlayerStatePresetSelected extends AudioPlayerStatePlaying {
  const AudioPlayerStatePresetSelected({required super.volume});
}

class AudioPlayerStateLacksPermissions extends AudioPlayerState {
  const AudioPlayerStateLacksPermissions();
}
