import 'package:flutter_background/flutter_background.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'audio_player_interface.dart';
import 'globals.dart';

part 'audio_player_state.dart';

class AudioPlayerCubit extends Cubit<AudioPlayerState> {
  final AudioPlayerInterface _audioPlayerInterface;

  AudioPlayerCubit(this._audioPlayerInterface)
      : super(AudioPlayerStateInitial());

  Future<void> play() async {
    // todo -- wrap parts/all of this in try-catch for PlatformException

    if (await FlutterBackground.hasPermissions) {
      await _audioPlayerInterface.play();

      emit(
        AudioPlayerStatePlaying(volume: state.volume),
      );
    } else {
      emit(
        AudioPlayerStateLacksPermissions(),
      );
    }
  }

  Future<void> setVolume(double volume) async {
    await _audioPlayerInterface.setVolume(volume);

    emit(
      AudioPlayerStatePlaying(volume: volume),
    );
  }

  Future<void> selectPreset(double volume) async {
    await setVolume(volume);
    await play();

    emit(
      AudioPlayerStatePlaying(volume: volume),
    );
  }

  Future<void> updatePreset(String storageBoxKey) async {
    await presetsBox.put(storageBoxKey, state.volume);
    emit(
      AudioPlayerStatePresetSelected(volume: state.volume),
    );
  }

  Future<void> stop() async {
    // todo -- wrap this in try-catch for PlatformException
    await _audioPlayerInterface.stop();

    emit(
      AudioPlayerStateInitial(),
    );
  }

  void enableScreensaver() {
    emit(
      AudioPlayerStatePlayingWithScreensaver(volume: state.volume),
    );
  }

  void disableScreensaver() {
    emit(
      AudioPlayerStatePlaying(volume: state.volume),
    );
  }

  void promptAboutScreensaver() {
    emit(
      AudioPlayerStateDisplayScreensaverInfo(),
    );
  }
}
