import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

const screensaverIcon = Icon(Icons.nightlight_round);

class Screensaver extends StatelessWidget {
  const Screensaver({super.key});

  @override
  Widget build(BuildContext context) => SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Container(
                color: Colors.black,
                child: Center(
                  child: ScreensaverText(),
                ),
              ),
            ),
          ],
        ),
      );
}

class ScreensaverText extends StatefulWidget {
  const ScreensaverText({super.key});

  @override
  State<ScreensaverText> createState() => _ScreensaverTextState();
}

class _ScreensaverTextState extends State<ScreensaverText> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.manual,
      overlays: [],
    );
    WakelockPlus.enable();
  }

  @override
  Widget build(BuildContext context) {
    Timer(Duration(minutes: 1), () {
      if (mounted) {
        setState(() {});
      }
    });
    final now = DateTime.now();
    return Text(
      '${now.hour}:${now.minute < 10 ? '0' : ''}${now.minute}',
      style: Theme.of(context).textTheme.displayMedium?.copyWith(
            color: Colors.white54,
          ),
    );
  }

  @override
  Future<void> deactivate() async {
    await SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.manual,
      overlays: SystemUiOverlay.values,
    );
    await WakelockPlus.disable();
    super.deactivate();
  }
}
