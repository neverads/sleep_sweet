import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:pubspec_version_checker/pubspec_version_checker.dart';
import 'package:sleep_sweet/globals.dart';

import 'audio_player_interface.dart';
import 'main_screen.dart';

void main() async {
  await Hive.initFlutter();
  presetsBox = await Hive.openBox("main_box");

  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(
          value: await AudioPlayerInterface.setup(),
        ),
      ],
      child: const SleepSweetApp(),
    ),
  );
}

class SleepSweetApp extends StatelessWidget {
  static const String title = 'Sleep Sweet';

  const SleepSweetApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.black,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.black,
          brightness: Brightness.dark,
          dynamicSchemeVariant: DynamicSchemeVariant.monochrome,
        ),
        snackBarTheme: SnackBarThemeData(
          backgroundColor: Colors.grey.shade900,
          contentTextStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(
            color: Colors.white60,
          ),
        ),
        useMaterial3: true,
      ),
      home: const PubspecVersionCheckerWidget(
        MainScreen(title),
        'https://gitlab.com/neverads/sleep_sweet/-/raw/main/pubspec.yaml',
        'https://gitlab.com/neverads/sleep_sweet/',
      ),
    );
  }
}
