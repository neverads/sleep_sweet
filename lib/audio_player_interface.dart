import 'package:flutter_background/flutter_background.dart';
import 'package:just_audio/just_audio.dart';

class AudioPlayerInterface {
  final AudioPlayer _player = AudioPlayer();

  AudioPlayerInterface._();

  static Future<AudioPlayerInterface> setup() async {
    final AudioPlayerInterface interface = AudioPlayerInterface._();

    await interface._player.setAsset('assets/brown_noise_15s_q0.ogg');
    await interface._player.setLoopMode(LoopMode.one);
    await interface._player.setVolume(0.0);

    return interface;
  }

  Future<void> play() async {
    await _player.play();

    if (!FlutterBackground.isBackgroundExecutionEnabled) {
      /// May throw a [PlatformException].
      await FlutterBackground.enableBackgroundExecution();
    }
  }

  Future<void> setVolume(double newVolume) async {
    await _player.setVolume(newVolume);
  }

  Future<void> stop() async {
    await _player.stop();

    if (FlutterBackground.isBackgroundExecutionEnabled) {
      /// May throw a [PlatformException].
      await FlutterBackground.disableBackgroundExecution();
    }
  }
}
