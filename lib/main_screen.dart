import 'package:flutter/material.dart';
import 'package:flutter_background/flutter_background.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sleep_sweet/audio_player_interface.dart';
import 'package:sleep_sweet/screensaver.dart';

import 'audio_player_cubit.dart';
import 'globals.dart';

const Color disabledIconColor = Colors.white24;

class MainScreen extends StatelessWidget {
  final String _title;

  const MainScreen(
    this._title, {
    super.key,
  });

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (BuildContext context) => AudioPlayerCubit(
          RepositoryProvider.of<AudioPlayerInterface>(context),
        ),
        child: _MainView(_title),
      );
}

class _MainView extends StatelessWidget {
  final String _title;

  const _MainView(this._title);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AudioPlayerCubit, AudioPlayerState>(
      builder: (BuildContext context, AudioPlayerState state) => Scaffold(
        appBar: state is AudioPlayerStatePlayingWithScreensaver
            ? null
            : AppBar(
                title: Text(_title),
                actions: [
                  IconButton(
                    icon: screensaverIcon,
                    onPressed: () async {
                      state is AudioPlayerStatePlaying
                          ? context.read<AudioPlayerCubit>().enableScreensaver()
                          : context
                              .read<AudioPlayerCubit>()
                              .promptAboutScreensaver();
                    },
                  ),
                ],
              ),
        body: Stack(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _sleepingStickFigure,
                  _volumeSlider(state.volume, context),
                  const SizedBox(height: 16),
                  _presets(context),
                ],
              ),
            ),
            state is AudioPlayerStatePlayingWithScreensaver
                ? GestureDetector(
                    onTap: context.read<AudioPlayerCubit>().disableScreensaver,
                    child: Screensaver(),
                  )
                : Container(),
          ],
        ),
      ),
      listener: (BuildContext context, AudioPlayerState state) async {
        if (state is AudioPlayerStateDisplayScreensaverInfo) {
          await _promptAboutScreensaver(context);
        } else if (state is AudioPlayerStateLacksPermissions) {
          await _promptAboutPermissions(context);
        } else if (state is AudioPlayerStatePlayingWithScreensaver) {
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(screensaverSnackBar);
        } else if (state is AudioPlayerStatePresetSelected) {
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(
            presetNewValueSnackBar(state.volume),
          );
        }
      },
    );
  }
}

final Widget _sleepingStickFigure = Row(
  children: [
    const Spacer(),
    Flexible(
      flex: 3,
      child: Image.asset(
        'assets/sleeping_stick_figure.png',
        color: Colors.white12,
      ),
    ),
    const Spacer(),
  ],
);

Widget _volumeSlider(double volume, BuildContext context) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Icon(
              Icons.volume_mute,
              color: disabledIconColor,
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 96,
              child: Slider(
                value: volume,
                onChangeStart: (double startValue) async {
                  await context.read<AudioPlayerCubit>().play();
                },
                onChanged: (double value) async {
                  await context.read<AudioPlayerCubit>().setVolume(value);
                },
                onChangeEnd: (double endValue) async {
                  if (endValue == 0) {
                    await context.read<AudioPlayerCubit>().stop();
                  }
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Icon(
              Icons.volume_up,
              color: disabledIconColor,
            ),
          ),
        ],
      ),
    );

Widget _presets(BuildContext context) => Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Flexible(
          flex: 2,
          child: _preset(
            StorageBoxKeys.lowPreset,
            0.15,
            context,
          ),
        ),
        Flexible(
          flex: 2,
          child: _preset(
            StorageBoxKeys.middlePreset,
            0.40,
            context,
          ),
        ),
        Flexible(
          flex: 2,
          child: _preset(
            StorageBoxKeys.highPreset,
            0.70,
            context,
          ),
        ),
      ],
    );

Widget _preset(
  String storageBoxKey,
  double defaultPercent,
  BuildContext context,
) {
  final double volume = presetsBox.get(
        storageBoxKey,
        defaultValue: defaultPercent,
      ) ??
      defaultPercent;
  return OutlinedButton(
    onPressed: () async =>
        await context.read<AudioPlayerCubit>().selectPreset(volume),
    onLongPress: () async =>
        await context.read<AudioPlayerCubit>().updatePreset(storageBoxKey),
    style: OutlinedButton.styleFrom(
      side: BorderSide(color: Colors.white12),
    ),
    child: Text(
      '${(volume * 100).round()}%',
      style: Theme.of(context).textTheme.titleLarge?.copyWith(
            color: Colors.white24,
          ),
    ),
  );
}

Future<void> _promptAboutPermissions(BuildContext context) async {
  await showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      const Color grey = Colors.grey;
      return AlertDialog(
        icon: const Icon(Icons.perm_device_info),
        iconColor: grey,
        title: const Text('Permission'),
        titleTextStyle:
            Theme.of(context).textTheme.titleLarge?.copyWith(color: grey),
        content: const SingleChildScrollView(
          child: Text(
            '"Sleep Sweet" needs your permission to run uninterrupted.\n'
            '\n'
            'Next, Android will display a prompt for permission.\n'
            '\n'
            'No funny business.  This app is free & open source (gitlab.com/neverads) with no ads or data-collection.',
          ),
        ),
        contentTextStyle:
            Theme.of(context).textTheme.titleSmall?.copyWith(color: grey),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('ok'),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
        backgroundColor: Color.alphaBlend(
          Colors.black45,
          Theme.of(context).primaryColor,
        ),
      );
    },
  );
  await FlutterBackground.initialize(
    androidConfig: FlutterBackgroundAndroidConfig(
      notificationTitle: "Sleep Sweet - white noise",
      notificationText: "Keeping the app awake to help you sleep.",
      // commented to use default
      // notificationIcon: AndroidResource(
      //   name: 'background_icon',
      //   defType: 'drawable',
      // ),
    ),
  );
}

SnackBar get screensaverSnackBar => SnackBar(
      content: const Text("Tap anywhere to return to the main screen."),
    );

SnackBar presetNewValueSnackBar(double newValue) => SnackBar(
      content: Text("New preset volume: ${(newValue * 100).round()}%."),
    );

Future<void> _promptAboutScreensaver(BuildContext context) async {
  return await showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      const Color primaryColor = Colors.grey;
      return AlertDialog(
        icon: screensaverIcon,
        iconColor: primaryColor,
        title: const Text('Nightlight'),
        titleTextStyle: Theme.of(context).textTheme.titleLarge?.copyWith(
              color: primaryColor,
            ),
        content: const SingleChildScrollView(
          child: Text(
            'When the app is actively playing white noise, '
            'this button will enable a clock/nightlight.',
          ),
        ),
        contentTextStyle: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: primaryColor,
            ),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('ok'),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
        backgroundColor: Color.alphaBlend(
          Colors.black45,
          Theme.of(context).primaryColor,
        ),
      );
    },
  );
}
