import 'package:hive_flutter/hive_flutter.dart';

// init/open box in `main`
late final Box<double> presetsBox;

class StorageBoxKeys {
  static const String lowPreset = "low_preset_key";
  static const String middlePreset = "medium_preset_key";
  static const String highPreset = "high_preset_key";
}


