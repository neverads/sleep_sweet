# 'Sleep Sweet' by Never Ads

Makes white-noise to help you sleep.

## Download the app here!

1. [Tap/click here to download](https://gitlab.com/neverads/sleep_sweet/-/raw/v4.1.1/build/app/outputs/flutter-apk/app-release.apk)
   the latest version for Android.
2. Open/install the file.
3. Open the app - that's it!

Everything from Never Ads is free & open source -- no ads & no data collection.
